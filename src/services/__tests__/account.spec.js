import { fetchLoginAccount, fetchRegisterAccount, fetchReadAccount, fetchLogoutAccount, fetchPswdAccount, fetchUpdateAccount, fetchDeleteAccount } from '../account';

describe('Account: Service', () => {
  it("Should 'fetchLoginAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          token: 'ABCDEF123456'
        })
      });
    }));

    const { token } = await fetchLoginAccount({
      login: 'Pickle',
      password: 'Azerty'
    });

    expect(global.fetch).toHaveBeenCalled();

    expect(token).toEqual('ABCDEF123456');
  });

  it("Should 'fetchLoginAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchLoginAccount({
        login: 'Pickle',
        password: 'Azerty'
      });
    } catch ({ message }) {
      expect(message).toEqual('Login Account Failure !');
    }
  });

  it("Should 'fetchRegisterAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          token: 'ABCDEF123456'
        })
      });
    }));

    const { token } = await fetchRegisterAccount({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      password: 'Azerty',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70
    });

    expect(global.fetch).toHaveBeenCalled();

    expect(token).toEqual('ABCDEF123456');
  });

  it("Should 'fetchRegisterAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchRegisterAccount({
        login: 'Pickle',
        email: 'rick.sanchez@pm.me',
        password: 'Azerty',
        firstName: 'Rick',
        lastName: 'Sanchez',
        gender: 'M',
        yearOld: 70
      });
    } catch ({ message }) {
      expect(message).toEqual('Register Account Failure !');
    }
  });

  it("Should 'fetchReadAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          login: 'Pickle',
          email: 'rick.sanchez@pm.me',
          firstName: 'Rick',
          lastName: 'Sanchez',
          gender: 'M',
          yearOld: 70
        })
      });
    }));

    const { login, email, firstName, lastName, gender, yearOld } = await fetchReadAccount('ABCDEF123456');

    expect(global.fetch).toHaveBeenCalled();

    expect(login).toEqual('Pickle');
    expect(email).toEqual('rick.sanchez@pm.me');
    expect(firstName).toEqual('Rick');
    expect(lastName).toEqual('Sanchez');
    expect(gender).toEqual('M');
    expect(yearOld).toEqual(70);
  });

  it("Should 'fetchReadAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchReadAccount('ABCDEF123456');
    } catch ({ message }) {
      expect(message).toEqual('Read Account Failure !');
    }
  });

  it("Should 'fetchLogoutAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          token: null
        })
      });
    }));

    const { token } = await fetchLogoutAccount('ABCDEF123456');

    expect(global.fetch).toHaveBeenCalled();

    expect(token).toBeNull();
  });

  it("Should 'fetchLogoutAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchLogoutAccount('ABCDEF123456');
    } catch ({ message }) {
      expect(message).toEqual('Logout Account Failure !');
    }
  });

  it("Should 'fetchPswdAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          updatedId: 'ABCDEF123456'
        })
      });
    }));

    const { updatedId } = await fetchPswdAccount('ABCDEF123456', {
      oldPswd: 'Azerty',
      newPswd: 'Qwerty'
    });

    expect(global.fetch).toHaveBeenCalled();

    expect(updatedId).toEqual('ABCDEF123456');
  });

  it("Should 'fetchPswdAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchPswdAccount('ABCDEF123456', {
        oldPswd: 'Azerty',
        newPswd: 'Qwerty'
      });
    } catch ({ message }) {
      expect(message).toEqual('Pswd Account Failure !');
    }
  });

  it("Should 'fetchUpdateAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          updatedId: 'ABCDEF123456'
        })
      });
    }));

    const { updatedId } = await fetchUpdateAccount('ABCDEF123456', {
      firstName: 'Rick',
      lastName: 'Sanchez'
    });

    expect(global.fetch).toHaveBeenCalled();

    expect(updatedId).toEqual('ABCDEF123456');
  });

  it("Should 'fetchUpdateAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchUpdateAccount('ABCDEF123456', {
        firstName: 'Rick',
        lastName: 'Sanchez'
      });
    } catch ({ message }) {
      expect(message).toEqual('Update Account Failure !');
    }
  });

  it("Should 'fetchDeleteAccount' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => ({
          deletedId: 'ABCDEF123456'
        })
      });
    }));

    const { deletedId } = await fetchDeleteAccount('ABCDEF123456');

    expect(global.fetch).toHaveBeenCalled();

    expect(deletedId).toEqual('ABCDEF123456');
  });

  it("Should 'fetchDeleteAccount' Throws Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 500
      });
    }));

    try {
      await fetchDeleteAccount('ABCDEF123456');
    } catch ({ message }) {
      expect(message).toEqual('Delete Account Failure !');
    }
  });
});
