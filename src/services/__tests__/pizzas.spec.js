import { fetchAllPizzas } from '../pizzas';

describe('Pizzas: Service', () => {
  it("Should 'fetchAllPizzas' Returns Response", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 200,
        json: () => [
          {
            id: 'ABCDEF123456',
            label: '4 Cheeses',
            items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
            price: 14.9
          }
        ]
      });
    }));

    const allPizzas = await fetchAllPizzas();

    expect(global.fetch).toHaveBeenCalled();

    expect(allPizzas).toHaveLength(1);
  });

  it("Should 'fetchAllPizzas' Returns Empty Response As Error", async () => {
    jest.spyOn(global, 'fetch').mockImplementationOnce(() => new Promise(resolve => {
      resolve({
        status: 400
      });
    }));

    const allPizzas = await fetchAllPizzas();

    expect(allPizzas).toHaveLength(0);
  });
});
