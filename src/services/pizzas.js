import api from './api';

/**
 * Fetch All Pizzas
 *
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchAllPizzas = () => {
  return fetch(api.pizzas(), {
    method: 'GET'
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    return [];
  });
};
