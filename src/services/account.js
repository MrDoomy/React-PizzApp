import api from './api';

/**
 * Fetch Login Account
 *
 * @param {Object} account Account
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchLoginAccount = account => {
  return fetch(api.login(), {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(account)
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Login Account Failure !');
  });
};

/**
 * Fetch Register Account
 *
 * @param {Object} account Account
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchRegisterAccount = account => {
  return fetch(api.register(), {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(account)
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Register Account Failure !');
  });
};

/**
 * Fetch Read Account
 *
 * @param {String} token Token
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchReadAccount = token => {
  return fetch(api.account(), {
    headers: {
      'Authorization': `Bearer ${token}`
    },
    method: 'GET'
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Read Account Failure !');
  });
};

/**
 * Fetch Logout Account
 *
 * @param {String} token Token
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchLogoutAccount = token => {
  return fetch(api.logout(), {
    headers: {
      'Authorization': `Bearer ${token}`
    },
    method: 'GET'
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Logout Account Failure !');
  });
};

/**
 * Fetch Pswd Account
 *
 * @param {String} token Token
 * @param {Object} credentials Credentials
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchPswdAccount = (token, credentials) => {
  return fetch(api.pswd(), {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: 'PUT',
    body: JSON.stringify(credentials)
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Pswd Account Failure !');
  });
};

/**
 * Fetch Update Account
 *
 * @param {String} token Token
 * @param {Object} account Account
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchUpdateAccount = (token, account) => {
  return fetch(api.account(), {
    headers: {
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    },
    method: 'PUT',
    body: JSON.stringify(account)
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Update Account Failure !');
  });
};

/**
 * Fetch Delete Account
 *
 * @param {String} token Token
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchDeleteAccount = token => {
  return fetch(api.account(), {
    headers: {
      'Authorization': `Bearer ${token}`
    },
    method: 'DELETE'
  }).then(response => {
    if (response.status >= 200 && response.status < 300) {
      return response.json();
    }

    throw new Error('Delete Account Failure !');
  });
};
