import { SET_TOKEN, SET_ACCOUNT, RESET_ACCOUNT } from './constants';

const initialState = {
  login: '',
  email: '',
  firstName: '',
  lastName: '',
  gender: '',
  yearOld: 0,
  token: null
};

export default function account(state = initialState, action = {}) {
  const { type, payload } = action;

  switch(type) {
    case SET_TOKEN:
      return {
        ...state,
        token: payload
      };

    case SET_ACCOUNT:
      return {
        ...state,
        ...payload
      };

    case RESET_ACCOUNT:
      return initialState;

    default:
      return state;
  }
}
