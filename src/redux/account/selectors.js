import { isEmpty } from '../../utils';

export const getAccount = state => state.account || {};

export const getLogin = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.login;
  }

  return '';
};

export const getEmail = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.email;
  }

  return '';
};

export const getFirstName = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.firstName;
  }

  return '';
};

export const getLastName = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.lastName;
  }

  return '';
};

export const getGender = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.gender;
  }

  return '';
};

export const getYearOld = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.yearOld;
  }

  return 0;
};

export const getToken = state => {
  const account = getAccount(state);

  if (!isEmpty(account)) {
    return account.token;
  }

  return null;
};
