import { SET_TOKEN, SET_ACCOUNT, RESET_ACCOUNT } from './constants';
import { getToken } from './selectors';
import { fetchLoginAccount, fetchReadAccount, fetchRegisterAccount, fetchLogoutAccount, fetchUpdateAccount, fetchPswdAccount, fetchDeleteAccount } from '../../services/account';

export const setToken = token => ({ type: SET_TOKEN, payload: token });

export const setAccount = account => ({ type: SET_ACCOUNT, payload: account });

export const resetAccount = () => ({ type: RESET_ACCOUNT });

export const loginAccount = ({ login, password }) => dispatch => {
  dispatch(setAccount({ login }));

  return fetchLoginAccount({ login, password })
    .then(({ token }) => {
      return dispatch(readAccount(token));
    })
    .catch(error => {
      dispatch(resetAccount());
      throw error;
    });
};

export const readAccount = token => dispatch => {
  dispatch(setToken(token));

  return fetchReadAccount(token)
    .then(account => {
      return dispatch(setAccount(account));
    })
    .catch(error => {
      dispatch(resetAccount());
      throw error;
    });
};

export const registerAccount = ({ password, ...account }) => dispatch => {
  dispatch(setAccount(account));

  return fetchRegisterAccount({ password, ...account })
    .then(({ token }) => {
      return dispatch(setToken(token));
    })
    .catch(error => {
      dispatch(resetAccount());
      throw error;
    });
};

export const logoutAccount = () => (dispatch, getState) => {
  const state = getState();
  const token = getToken(state);

  return fetchLogoutAccount(token)
    .then(() => dispatch(resetAccount()))
    .catch(() => dispatch(setToken(null)));
};

export const updateAccount = account => (dispatch, getState) => {
  const state = getState();
  const token = getToken(state);

  return fetchUpdateAccount(token, account)
    .then(({ updatedId }) => {
      if (!updatedId) {
        throw new Error('No ID');
      }

      return dispatch(setAccount(account));
    })
    .catch(error => {
      throw error;
    });
};

export const pswdAccount = credentials => (_, getState) => {
  const state = getState();
  const token = getToken(state);

  return fetchPswdAccount(token, credentials)
    .then(({ updatedId }) => {
      if (!updatedId) {
        throw new Error('No ID');
      }

      return;
    })
    .catch(error => {
      throw error;
    });
};

export const deleteAccount = () => (dispatch, getState) => {
  const state = getState();
  const token = getToken(state);

  return fetchDeleteAccount(token)
    .then(({ deletedId }) => {
      if (!deletedId) {
        throw new Error('No ID');
      }

      return dispatch(resetAccount());
    })
    .catch(error => {
      throw error;
    });
};
