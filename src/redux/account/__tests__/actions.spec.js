import * as Actions from '../actions';

describe('Account: Actions', () => {
  it("Should 'setToken' Returns 'type' & 'payload'", () => {
    expect(Actions.setToken('ABCDEF123456')).toEqual({
      type: '[Account] Set Token',
      payload: 'ABCDEF123456'
    });
  });

  it("Should 'setAccount' Returns 'type' & 'payload'", () => {
    const payload = {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Lorem',
      lastName: 'Ipsum',
      gender: 'M',
      yearOld: 70
    };

    expect(Actions.setAccount(payload)).toEqual({
      type: '[Account] Set Account',
      payload
    });
  });

  it("Should 'resetAccount' Returns 'type'", () => {
    expect(Actions.resetAccount()).toEqual({
      type: '[Account] Reset Account'
    });
  });
});
