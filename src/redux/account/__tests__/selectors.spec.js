import * as Selectors from '../selectors';

describe('Account: Selectors', () => {
  const state = {
    account: {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    }
  };

  it("Should 'getAccount' Returns State", () => {
    expect(Selectors.getAccount({})).toEqual({});
    expect(Selectors.getAccount(state)).toEqual({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    });
  });

  it("Should 'getLogin' Returns 'login'", () => {
    expect(Selectors.getLogin({})).toBeFalsy();
    expect(Selectors.getLogin(state)).toEqual('Pickle');
  });

  it("Should 'getEmail' Returns 'email'", () => {
    expect(Selectors.getEmail({})).toBeFalsy();
    expect(Selectors.getEmail(state)).toEqual('rick.sanchez@pm.me');
  });

  it("Should 'getFirstName' Returns 'firstName'", () => {
    expect(Selectors.getFirstName({})).toBeFalsy();
    expect(Selectors.getFirstName(state)).toEqual('Rick');
  });

  it("Should 'getLastName' Returns 'lastName'", () => {
    expect(Selectors.getLastName({})).toBeFalsy();
    expect(Selectors.getLastName(state)).toEqual('Sanchez');
  });

  it("Should 'getGender' Returns 'gender'", () => {
    expect(Selectors.getGender({})).toBeFalsy();
    expect(Selectors.getGender(state)).toEqual('M');
  });

  it("Should 'getYearOld' Returns 'yearOld'", () => {
    expect(Selectors.getYearOld({})).toBeFalsy();
    expect(Selectors.getYearOld(state)).toEqual(70);
  });

  it("Should 'getToken' Returns 'token'", () => {
    expect(Selectors.getToken({})).toBeFalsy();
    expect(Selectors.getToken(state)).toEqual('ABCDEF123456');
  });
});