import * as Actions from '../actions';

describe('Pizzas: Actions', () => {
  it("Should 'setPizzas' Returns 'type' & 'payload'", () => {
    const payload = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    expect(Actions.setPizzas(payload)).toEqual({
      type: '[Pizzas] Set Pizzas',
      payload
    });
  });

  it("Should 'addPizza' Returns 'type' & 'payload'", () => {
    const payload = {
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    expect(Actions.addPizza(payload)).toEqual({
      type: '[Pizzas] Add Pizza',
      payload
    });
  });

  it("Should 'upPizza' Returns 'type' & 'payload'", () => {
    const payload = {
      id: 'ABCDEF123456',
      label: '3 Cheeses',
      items: ['Mozzarella', 'Reblochon', 'Gorgonzola'],
      price: 9.9
    };

    expect(Actions.upPizza(payload)).toEqual({
      type: '[Pizzas] Up Pizza',
      payload
    });
  });

  it("Should 'delPizza' Returns 'type' & 'payload'", () => {
    expect(Actions.delPizza('ABCDEF123456')).toEqual({
      type: '[Pizzas] Del Pizza',
      payload: 'ABCDEF123456'
    });
  });

  it("Should 'resetPizzas' Returns 'type'", () => {
    expect(Actions.resetPizzas()).toEqual({
      type: '[Pizzas] Reset Pizzas'
    });
  });
});
