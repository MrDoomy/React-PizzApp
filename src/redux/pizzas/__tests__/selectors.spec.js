import * as Selectors from '../selectors';

describe('Pizzas: Selectors', () => {
  const state = {
    pizzas: [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ]
  };

  it("Should 'getPizzas' Returns State", () => {
    expect(Selectors.getPizzas({})).toHaveLength(0);
    expect(Selectors.getPizzas(state)).toHaveLength(1);
  });

  it("Should 'getPizzaById' Returns 'pizza'", () => {
    expect(Selectors.getPizzaById('ABCDEF123456')(state)).toEqual({
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    });
  });
});