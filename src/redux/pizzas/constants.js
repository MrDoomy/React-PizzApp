export const SET_PIZZAS = '[Pizzas] Set Pizzas';
export const ADD_PIZZA = '[Pizzas] Add Pizza';
export const UP_PIZZA = '[Pizzas] Up Pizza';
export const DEL_PIZZA = '[Pizzas] Del Pizza';
export const RESET_PIZZAS = '[Pizzas] Reset Pizzas';
