import { SET_PIZZAS, ADD_PIZZA, UP_PIZZA, DEL_PIZZA, RESET_PIZZAS } from './constants';
import { getPizzas } from './selectors';
import { fetchAllPizzas } from '../../services/pizzas';
import { sortByKey } from '../../utils';

export const setPizzas = pizzas => ({ type: SET_PIZZAS, payload: pizzas });

export const addPizza = pizza => ({ type: ADD_PIZZA, payload: pizza });

export const upPizza = pizza => ({ type: UP_PIZZA, payload: pizza });

export const delPizza = id => ({ type: DEL_PIZZA, payload: id });

export const resetPizzas = () => ({ type: RESET_PIZZAS });

export const allPizzas = key => (dispatch, getState) => {
  const state = getState();
  const pizzas = getPizzas(state);

  if (pizzas.length === 0) {
    fetchAllPizzas()
      .then(allPizzas => {
        const sortedByLabel = allPizzas.sort(sortByKey(key));
    
        dispatch(setPizzas(sortedByLabel));
      });
  }
};
