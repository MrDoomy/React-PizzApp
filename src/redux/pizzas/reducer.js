import { SET_PIZZAS, ADD_PIZZA, UP_PIZZA, DEL_PIZZA, RESET_PIZZAS } from './constants';

const initialState = [];

export default function pizzas(state = initialState, action = {}) {
  const { type, payload } = action;

  switch(type) {
    case SET_PIZZAS:
      return payload;

    case ADD_PIZZA:
      return [
        ...state,
        payload
      ];

    case UP_PIZZA:
      return state.map(pizza => pizza.id === payload.id ? payload : pizza);

    case DEL_PIZZA:
      return state.filter(pizza => pizza.id !== payload);

    case RESET_PIZZAS:
      return initialState;

    default:
      return state;
  }
}
