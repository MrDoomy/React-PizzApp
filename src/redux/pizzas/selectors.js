export const getPizzas = state => state.pizzas || [];

export const getPizzaById = id => state => getPizzas(state).find(pizza => pizza.id === id);
