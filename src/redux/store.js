import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import account from './account';
import pizzas from './pizzas';

// OPTIONAL: Get Store Item (From Session Storage) 
const storeItem = sessionStorage.getItem('store');

const rootReducer = combineReducers({
  account,
  pizzas
});

const persistedState = storeItem ? JSON.parse(storeItem) : {};

export const makeStore = (state = persistedState) => {
  const composeEnhancers = composeWithDevTools(applyMiddleware(thunk));

  return createStore(rootReducer, state, composeEnhancers);
};

const store = makeStore();

/**
 * OPTIONAL: Set Store Item (In Session Store)
 */
store.subscribe(() => {
  sessionStorage.setItem('store', JSON.stringify(store.getState()));
});

export default store;
