import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { allPizzas } from '../redux/pizzas';
import { NavBar } from './containers';
import Routing from './Routing';

function App() {
  const dispatch = useDispatch();
 
  useEffect(() => {
    console.log('Mounted !');

    dispatch(allPizzas('label'));
  }, [dispatch]);

  return (
    <> 
      <NavBar />
      <Routing />
    </>
  );
}

export default App;
