import React, { useEffect, useState } from 'react';
import { func } from 'prop-types';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getLogin } from '../../redux/account';
import { getPizzas, delPizza } from '../../redux/pizzas';
import { Row, Column, Button } from '../ui';
import icon from '../../assets/icon.png';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [
    value,
    e => setValue(e.target.value)
  ];
}

function useMatrix(breakpoint) {
  const [values, setValues] = useState([]);

  const getValues = (filter = '') => {
    filter = filter.toLowerCase();

    const filteredValues = values.filter(val => {
      const label = val['label'].toLowerCase();

      return label.includes(filter);
    });

    let outerVal = [];

    // Rows
    for (let i = 0; i < filteredValues.length; i += breakpoint) {
      let innerVal = [];

      // Cols
      for (let j = i; j < i + breakpoint; j++) {
        innerVal = [
          ...innerVal,
          filteredValues[j]
        ];
      }

      outerVal = [
        ...outerVal,
        innerVal
      ];
    }

    return outerVal;
  };

  return [
    getValues,
    setValues
  ];
}

function Home(props) {
  const dispatch = useDispatch();
  const isRoot = useSelector(state => getLogin(state) === 'Pickle');
  const allPizzas = useSelector(getPizzas);
  const [search, setSearch] = useInput('');
  const [getMatrix, setMatrix] = useMatrix(4);

  useEffect(() => {
    setMatrix(allPizzas);
  }, [allPizzas, setMatrix]);

  const handleClick = pizza => {
    const { setMessage, colorizeYellow } = props;

    dispatch(delPizza(pizza.id));
    colorizeYellow();
    setMessage(`${pizza.label} Removed !`);
  };

  return (
    <>
      <section className="hero">
        <div className="hero-body" style={{ padding: '3rem 1.5rem 0' }}>
          <div className="container">
            <Row>
              <Column size={6} offset={3}>
                <div className="control has-icons-right">
                  <input className="input is-rounded" type="text" placeholder="Filter" defaultValue={search} onChange={setSearch} />
                  <span className="icon is-small is-right">
                    <i className="fas fa-search" />
                  </span>
                </div>
              </Column>
            </Row>
          </div>
        </div>
      </section>

      <section className="section">
        <div className="container">
          {getMatrix(search).map((pizzas, i) => (
            <Row key={i}>
              {pizzas.map((pizza, j) => {
                if (pizza) {
                  const { id, label, items, price } = pizza;

                  return (
                    <Column key={j}>
                      <div className="card">
                        <div className="card-content">
                          <div className="media">
                            <div className="media-left">
                              <figure className="image is-48x48">
                                <img src={icon} alt={label} />
                              </figure>
                            </div>
                            <div className="media-content">
                              <p className="title is-4">{label}</p>
                              <p className="subtitle is-6">{price.toFixed(2)} €</p>
                            </div>
                          </div>
                          <div className="content">
                            {items.length} Items...
                          </div>
                        </div>

                        <footer className="card-footer">
                          <Link to={`/pizza/${id}`} className="card-footer-item">
                            Details
                          </Link>

                          {isRoot && (
                            <div className="card-footer-item">
                              <Button color="danger" outlined onClick={() => handleClick(pizza)}>
                                Remove
                              </Button>
                            </div>
                          )}
                        </footer>
                      </div>
                    </Column>
                  );
                }

                return <Column key={j} />;
              })}
            </Row>
          ))}
        </div>
      </section>
    </>
  );
}

Home.propTypes = {
  setMessage: func.isRequired,
  colorizeYellow: func.isRequired
};

export default Home;
