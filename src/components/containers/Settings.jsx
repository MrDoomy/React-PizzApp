import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getToken } from '../../redux/account';
import Profile from './Profile';
import Credentials from './Credentials';
import { Row, Column, Notification } from '../ui';

function useRedirect(notAllowed, pathName) {
  const history = useHistory();

  useEffect(() => {
    if (notAllowed) {
      history.push(pathName);
    }
  }, [history, notAllowed, pathName]);
}

function Settings() {
  const isConnected = useSelector(state => !!getToken(state));

  useRedirect(!isConnected, '/login');

  return (
    <section className="section">
      <div className="container">
        <Row>
          <Column size={6} offset={3}>
            <Notification render={props => <Profile {...props} />}/>
          </Column>
        </Row>
        <Row>
          <Column size={6} offset={3}>
            <Notification render={props => <Credentials {...props} />}/>
          </Column>
        </Row>
      </div>
    </section>
  );
}

export default Settings;
