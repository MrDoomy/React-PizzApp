import React, { useState, useReducer } from 'react';
import { func } from 'prop-types';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { getAccount, updateAccount } from '../../redux/account';
import { Row, Column, Field, Button } from '../ui';

function reducer(state, { type, payload }) {
  switch (type) {
    case 'set':
      return {
        ...state,
        ...payload
      };

    case 'reset':
      return {
        ...state,
        [payload]: ''
      };

    default:
      return state;
  }
}

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [
    value,
    e => setValue(e.target.value)
  ];
}

function Profile(props) {
  const dispatch = useDispatch();
  const account = useSelector(getAccount);
  const [hidden, setHidden] = useState(true);
  const [email, setEmail] = useInput(account.email);
  const [firstName, setFirstName] = useInput(account.firstName);
  const [lastName, setLastName] = useInput(account.lastName);
  const [gender, setGender] = useInput(account.gender);
  const [yearOld, setYearOld] = useInput(account.yearOld);
  const [errors, dispatchErrors] = useReducer(reducer, {
    email: '',
    firstName: '',
    lastName: '',
    gender: '',
    yearOld: ''
  });

  const handleSubmit = event => {
    const { setMessage, colorizeRed, colorizeGreen } = props;

    event.preventDefault();
    
    if (!email) {
      return dispatchErrors({ type: 'set', payload: { email: 'Email Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'email' });
    
    if (!firstName || firstName.length < 2 || firstName.length > 16) {
      return dispatchErrors({ type: 'set', payload: { firstName: 'First Name Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'firstName' });
    
    if (!lastName || lastName.length < 2 || lastName.length > 16) {
      return dispatchErrors({ type: 'set', payload: { lastName: 'Last Name Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'lastName' });
    
    if (!gender) {
      return dispatchErrors({ type: 'set', payload: { gender: 'Gender Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'gender' });
    
    if (!yearOld || yearOld < 1 || yearOld > 99) {
      return dispatchErrors({ type: 'set', payload: { yearOld: 'Year Old Error !' }});
    }

    dispatchErrors({ type: 'reset', payload: 'yearOld' });

    dispatch(updateAccount({
      email,
      firstName,
      lastName,
      gender,
      yearOld
    }))
      .then(() => {
        colorizeGreen();
        setMessage('Success !');
      })
      .catch(({ message }) => {
        colorizeRed();
        setMessage(message);
      });
  };

  return (
    <div className="card">
      <header className="card-header">
        <p className="card-header-title">
          Profile
        </p>
        <a className="card-header-icon" href="#profile" aria-label="toggle" onClick={() => setHidden(!hidden)}>
          <span className="icon">
            <i className="fas fa-angle-down" />
          </span>
        </a>
      </header>

      <div id="profile" className={classNames('card-content', { 'is-hidden': hidden })}>
        <form onSubmit={handleSubmit}>
          <Field
            label="Email"
            name="email"
            type="email"
            placeholder="rick.sanchez@pm.me"
            defaultValue={email}
            onChange={setEmail}
            error={errors.email} />

          <Field
            label="First Name"
            name="firstName"
            placeholder="Rick"
            defaultValue={firstName}
            onChange={setFirstName}
            error={errors.firstName} />

          <Field
            label="Last Name"
            name="lastName"
            placeholder="Sanchez"
            defaultValue={lastName}
            onChange={setLastName}
            error={errors.lastName} />

          <div className="field">
            <label className="label">Gender</label>
            <div className="control">
              <div className={classNames('select', 'is-fullwidth', { 'is-danger': errors.gender })}>
                <select name="gender" defaultValue={gender} onChange={setGender}>
                  <option value="" disabled>Unknown</option>
                  <option value="M">Male</option>
                  <option value="F">Female</option>
                </select>
              </div>
            </div>
            {errors.gender && <p className="help is-danger">{errors.gender}</p>}
          </div>

          <Field
            label="Year Old"
            name="yearOld"
            type="number"
            placeholder="70"
            defaultValue={yearOld}
            onChange={setYearOld}
            error={errors.yearOld} />

          <Row>
            <Column size={6} offset={3}>
              <Button type="submit" color="success" filled>
                <strong>Save</strong>
              </Button>
            </Column>
          </Row>
        </form>
      </div>
    </div>
  );
}

Profile.propTypes = {
  setMessage: func.isRequired,
  colorizeRed: func.isRequired,
  colorizeGreen: func.isRequired
};

export default Profile;
