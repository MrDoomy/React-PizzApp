import React, { useState } from 'react';
import { bool } from 'prop-types';
import classNames from 'classnames';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getToken, getFirstName, logoutAccount } from '../../redux/account';
import { Button } from '../ui';
import logo from '../../assets/logo.png';

function NavBar(props) {
  const { elevated, spaced } = props;

  const history = useHistory();
  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const isConnected = useSelector(state => !!getToken(state));
  const firstName = useSelector(getFirstName);
  const [active, setActive] = useState(false);

  const handleClick = () => {
    dispatch(logoutAccount()).then(() => {
      if (pathname !== '/') {
        history.push('/');
      }
    });
  };

  return (
    <nav className={classNames('navbar', { 'has-shadow': elevated, 'is-spaced': spaced })} role="navigation" aria-label="main navigation">
      <div className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item">
            <img src={logo} width="112" height="28" alt="PizzApp" />
          </Link>

          <a href="#menu" className={classNames('navbar-burger', 'burger', { 'is-active': active })} role="button" aria-label="menu" aria-expanded="false" data-target="menu" onClick={() => setActive(!active)}>
            <span aria-hidden="true" />
            <span aria-hidden="true" />
            <span aria-hidden="true" />
          </a>
        </div>
    
        <div id="menu" className={classNames('navbar-menu', { 'is-active': active })}>
          {isConnected && (
            <div className="navbar-start">
              <Link to="/settings" className="navbar-item">
                <span className="icon">
                  <i className="fas fa-user-secret" />
                </span>
                <span>Hello {firstName}</span>
              </Link>
            </div>
          )}

          <div className="navbar-end">
            <div className="navbar-item">
              {isConnected ? (
                <div className="buttons">
                  <Button color="warning" onClick={handleClick}>
                    <strong>Logout</strong>
                  </Button>
                </div>
              ) : (
                <div className="buttons">
                  <Link to="/register" className="button is-primary">
                    <strong>Register</strong>
                  </Link>

                  <Link to="/login" className="button is-light">
                    Login
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

NavBar.defaultProps = {
  elevated: true,
  spaced: true
};

NavBar.propTypes = {
  elevated: bool,
  spaced: bool
};

export default NavBar;
