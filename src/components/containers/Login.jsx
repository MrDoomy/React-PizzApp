import React, { useEffect, useState } from 'react';
import { func } from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getToken, loginAccount } from '../../redux/account';
import { Row, Column, Field, Button } from '../ui';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [
    value,
    e => setValue(e.target.value)
  ];
}

function useRedirect(notAllowed, pathName) {
  const history = useHistory();

  useEffect(() => {
    if (notAllowed) {
      history.push(pathName);
    }
  }, [history, notAllowed, pathName]);
}

function Login(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const isConnected = useSelector(state => !!getToken(state));
  const [login, setLogin] = useInput('');
  const [loginFail, setLoginFail] = useState('');
  const [password, setPassword] = useInput('');
  const [passwordFail, setPasswordFail] = useState('');

  useRedirect(isConnected, '/');

  const handleSubmit = event => {
    const { setMessage, colorizeRed } = props;

    event.preventDefault();

    if (!login || login.length < 4 || login.length > 32) {
      return setLoginFail('Login Error !');
    }

    setLoginFail('');
    
    if (!password || password.length < 6) {
      return setPasswordFail('Password Error !');
    }

    setPasswordFail('');

    dispatch(loginAccount({ login, password }))
      .then(() => history.push('/'))
      .catch(({ message }) => {
        colorizeRed();
        setMessage(message);
      });
  };

  return (
    <section className="section">
      <div className="container">
        <Row>
          <Column size={6} offset={3}>
            <div className="card">
              <div className="card-content">
                <form onSubmit={handleSubmit}>
                  <Field
                    label="Login"
                    name="login"
                    placeholder="Pickle"
                    defaultValue={login}
                    onChange={setLogin}
                    error={loginFail} />

                  <Field
                    label="Password"
                    name="password"
                    type="password"
                    placeholder="**********"
                    defaultValue={password}
                    onChange={setPassword}
                    error={passwordFail} />

                  <Row>
                    <Column size={6} offset={3}>
                      <Button type="submit" lighten filled>
                        Login
                      </Button>
                    </Column>
                  </Row>
                </form>
              </div>
            </div>
          </Column>
        </Row>
      </div>
    </section>
  );
}

Login.propTypes = {
  setMessage: func.isRequired,
  colorizeRed: func.isRequired
};

export default Login;
