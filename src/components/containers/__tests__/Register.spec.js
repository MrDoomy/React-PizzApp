import React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithReduxAndRouter } from '../../../testUtils';
import { Notification } from '../../ui';
import Register from '../Register';

describe('Register: Component', () => {
  it('Should Component Renders', () => {
    const { queryByPlaceholderText } = renderWithReduxAndRouter(
      <Notification render={props => <Register {...props} />} />,
      {
        account: {
          token: null
        }
      }
    );

    expect(queryByPlaceholderText('Rick')).toBeInTheDocument();
    expect(queryByPlaceholderText('Sanchez')).toBeInTheDocument();
  });

  it('Should Form Validation Displays Error', () => {
    const { getByPlaceholderText, getByRole, queryByText } = renderWithReduxAndRouter(
      <Notification render={props => <Register {...props} />} />,
      {
        account: {
          token: null
        }
      }
    );

    const email = getByPlaceholderText('rick.sanchez@pm.me');

    fireEvent.change(email, { target: { value: 'rick.sanchez' } });

    const yearOld = getByPlaceholderText('70');

    fireEvent.change(yearOld, { target: { value: 100 } });

    const button = getByRole('button');

    fireEvent.click(button);

    expect(queryByText('Login Error !')).toBeInTheDocument();
  });
});
