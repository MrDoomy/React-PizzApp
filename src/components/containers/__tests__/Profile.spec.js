import React from 'react';
import { fireEvent } from '@testing-library/react';
import { renderWithReduxAndRouter } from '../../../testUtils';
import { Notification } from '../../ui';
import Profile from '../Profile';

describe('Profile: Component', () => {
  it('Should Component Renders', () => {
    const { queryByDisplayValue } = renderWithReduxAndRouter(
      <Notification render={props => <Profile {...props} />} />,
      {
        account: {
          email: 'rick.sanchez@pm.me',
          firstName: 'Rick',
          lastName: 'Sanchez',
          gender: 'M',
          yearOld: 70,
          token: 'ABCDEF123456'
        }
      }
    );

    expect(queryByDisplayValue('Rick')).toBeInTheDocument();
    expect(queryByDisplayValue('Sanchez')).toBeInTheDocument();
  });

  it('Should Form Validation Displays Error', () => {
    const { getByDisplayValue, getByRole, queryByText } = renderWithReduxAndRouter(
      <Notification render={props => <Profile {...props} />} />,
      {
        account: {
          email: 'rick.sanchez@pm.me',
          firstName: 'Rick',
          lastName: 'Sanchez',
          gender: 'M',
          yearOld: 70,
          token: 'ABCDEF123456'
        }
      }
    );

    const input = getByDisplayValue('rick.sanchez@pm.me');

    fireEvent.change(input, { target: { value: '' } });

    const button = getByRole('button');

    fireEvent.click(button);

    expect(queryByText('Email Error !')).toBeInTheDocument();
  });
});
