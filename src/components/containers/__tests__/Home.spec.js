import React from 'react';
import { renderWithReduxAndRouter } from '../../../testUtils';
import { Notification } from '../../ui';
import Home from '../Home';
import { fireEvent } from '@testing-library/react';

describe('Home: Component', () => {
  it('Should Component Renders', () => {
    const { queryByText } = renderWithReduxAndRouter(
      <Notification render={props => <Home {...props} />} />,
      {
        pizzas: [
          {
            id: 'ABCDEF123456',
            label: '4 Cheeses',
            items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
            price: 14.9
          },
          {
            id: 'ABCDEF123457',
            label: 'Atlas',
            items: ['Mozzarella', 'Spicy Chicken', 'Chorizo', 'Marinated Zucchini', 'Ras-El-Hanout Spices'],
            price: 13.6
          },
          {
            id: 'ABCDEF123458',
            label: 'Barcelona',
            items: ['Mozzarella', 'Chorizo', 'Fried Onions', 'Peppers', 'Olives'],
            price: 13.3
          },
          {
            id: 'ABCDEF123459',
            label: 'Basque',
            items: ['Mozzarella', 'Spicy Chicken', 'Peppers', 'Candied Tomatoes'],
            price: 13.6
          }
        ]
      }
    );

    expect(queryByText('4 Cheeses')).toBeInTheDocument();
    expect(queryByText('Atlas')).toBeInTheDocument();
    expect(queryByText('Barcelona')).toBeInTheDocument();
    expect(queryByText('Basque')).toBeInTheDocument();
  });

  it('Should Filter Input Works', () => {
    const { getByPlaceholderText, queryByText } = renderWithReduxAndRouter(
      <Notification render={props => <Home {...props} />} />,
      {
        pizzas: [
          {
            id: 'ABCDEF123456',
            label: '4 Cheeses',
            items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
            price: 14.9
          },
          {
            id: 'ABCDEF123457',
            label: 'Atlas',
            items: ['Mozzarella', 'Spicy Chicken', 'Chorizo', 'Marinated Zucchini', 'Ras-El-Hanout Spices'],
            price: 13.6
          },
          {
            id: 'ABCDEF123458',
            label: 'Barcelona',
            items: ['Mozzarella', 'Chorizo', 'Fried Onions', 'Peppers', 'Olives'],
            price: 13.3
          },
          {
            id: 'ABCDEF123459',
            label: 'Basque',
            items: ['Mozzarella', 'Spicy Chicken', 'Peppers', 'Candied Tomatoes'],
            price: 13.6
          }
        ]
      }
    );

    const input = getByPlaceholderText('Filter');

    fireEvent.change(input, { target: { value: 'Ba' }});

    expect(queryByText('4 Cheeses')).not.toBeInTheDocument();
    expect(queryByText('Atlas')).not.toBeInTheDocument();
    expect(queryByText('Barcelona')).toBeInTheDocument();
    expect(queryByText('Basque')).toBeInTheDocument();
  });
});
