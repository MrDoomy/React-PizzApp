
import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { useHistory, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { getPizzaById } from '../../redux/pizzas';
import { Row, Column } from '../ui';

function Pizza() {
  const history = useHistory();
  const { id } = useParams();
  const pizza = useSelector(getPizzaById(id));
  const [hidden, setHidden] = useState(false);
  
  useEffect(() => {
    if (!pizza) {
      history.push('/');
    }
  }, [history, pizza]);

  return (
    <section className="section">
      <div className="container">
        <Row>
          <Column size={6} offset={3}>
            {pizza && (
              <div className="card">
                <header className="card-header">
                  <p className="card-header-title">
                    {pizza.label}
                  </p>
                  <a className="card-header-icon" href="#items" aria-label="toggle" onClick={() => setHidden(!hidden)}>
                    <span className="icon">
                      <i className="fas fa-angle-down" />
                    </span>
                  </a>
                </header>

                <div id="items" className={classNames('card-content', { 'is-hidden': hidden })}>
                  <div className="content">
                    <table className="table is-hoverable">
                      <tbody>
                        {pizza.items.map((item, idx) => (
                          <tr key={idx}>
                            <td>{item}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            )}
          </Column>
        </Row>
      </div>
    </section>
  );
}

export default Pizza;
