import React, { useEffect, useState } from 'react';
import { func } from 'prop-types';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getToken, registerAccount } from '../../redux/account';
import { Row, Column, Field, Button } from '../ui';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [
    value,
    e => setValue(e.target.value)
  ];
}

function useRedirect(notAllowed, pathName) {
  const history = useHistory();

  useEffect(() => {
    if (notAllowed) {
      history.push(pathName);
    }
  }, [history, notAllowed, pathName]);
}

function Register(props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const isConnected = useSelector(state => !!getToken(state));
  const [login, setLogin] = useInput('');
  const [loginFail, setLoginFail] = useState('');
  const [password, setPassword] = useInput('');
  const [passwordFail, setPasswordFail] = useState('');
  const [repeatPassword, setRepeatPassword] = useInput('');
  const [repeatPasswordFail, setRepeatPasswordFail] = useState('');
  const [email, setEmail] = useInput('');
  const [emailFail, setEmailFail] = useState('');
  const [firstName, setFirstName] = useInput('');
  const [firstNameFail, setFirstNameFail] = useState('');
  const [lastName, setLastName] = useInput('');
  const [lastNameFail, setLastNameFail] = useState('');
  const [gender, setGender] = useInput('');
  const [genderFail, setGenderFail] = useState('');
  const [yearOld, setYearOld] = useInput(0);
  const [yearOldFail, setYearOldFail] = useState('');

  useRedirect(isConnected, '/');

  const handleSubmit = event => {
    const { setMessage, colorizeRed } = props;

    event.preventDefault();

    if (!login || login.length < 4 || login.length > 32) {
      return setLoginFail('Login Error !');
    }

    setLoginFail('');
    
    if (!password || password.length < 6) {
      return setPasswordFail('Password Error !');
    }

    setPasswordFail('');
    
    if (!repeatPassword || repeatPassword.length < 6 || repeatPassword !== password) {
      return setRepeatPasswordFail('Repeat Password Error !');
    }

    setRepeatPasswordFail('');
    
    if (!email) {
      return setEmailFail('Email Error !');
    }

    setEmailFail('');
    
    if (!firstName || firstName.length < 2 || firstName.length > 16) {
      return setFirstNameFail('First Name Error !');
    }

    setFirstNameFail('');
    
    if (!lastName || lastName.length < 2 || lastName.length > 16) {
      return setLastNameFail('Last Name Error !');
    }

    setLastNameFail('');
    
    if (!gender) {
      return setGenderFail('Gender Error !');
    }

    setGenderFail('');
    
    if (!yearOld || yearOld < 1 || yearOld > 99) {
      return setYearOldFail('Year Old Error !');
    }

    setYearOldFail('');

    dispatch(registerAccount({
      login,
      password,
      email,
      firstName,
      lastName,
      gender,
      yearOld
    }))
      .then(() => history.push('/'))
      .catch(({ message }) => {
        colorizeRed();
        setMessage(message);
      });
  };

  return (
    <section className="section">
      <div className="container">
        <Row>
          <Column size={6} offset={3}>
            <div className="card">
              <div className="card-content">
                <form onSubmit={handleSubmit}>
                  <Field
                    label="Login"
                    name="login"
                    placeholder="Pickle"
                    defaultValue={login}
                    onChange={setLogin}
                    error={loginFail} />

                  <Field
                    label="Password"
                    name="password"
                    type="password"
                    placeholder="**********"
                    defaultValue={password}
                    onChange={setPassword}
                    error={passwordFail} />

                  <Field
                    label="Repeat Password"
                    name="repeatPassword"
                    type="password"
                    placeholder="**********"
                    defaultValue={repeatPassword}
                    onChange={setRepeatPassword}
                    error={repeatPasswordFail} />

                  <Field
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="rick.sanchez@pm.me"
                    defaultValue={email}
                    onChange={setEmail}
                    error={emailFail} />

                  <Field
                    label="First Name"
                    name="firstName"
                    placeholder="Rick"
                    defaultValue={firstName}
                    onChange={setFirstName}
                    error={firstNameFail} />

                  <Field
                    label="Last Name"
                    name="lastName"
                    placeholder="Sanchez"
                    defaultValue={lastName}
                    onChange={setLastName}
                    error={lastNameFail} />

                  <div className="field">
                    <label className="label">Gender</label>
                    <div className="control">
                      <div className={classNames('select', 'is-fullwidth', { 'is-danger': genderFail })}>
                        <select name="gender" defaultValue={gender} onChange={setGender}>
                          <option value="" disabled>Unknown</option>
                          <option value="M">Male</option>
                          <option value="F">Female</option>
                        </select>
                      </div>
                    </div>
                    {genderFail && <p className="help is-danger">{genderFail}</p>}
                  </div>

                  <Field
                    label="Year Old"
                    name="yearOld"
                    type="number"
                    placeholder="70"
                    defaultValue={yearOld}
                    onChange={setYearOld}
                    error={yearOldFail} />

                  <Row>
                    <Column size={6} offset={3}>
                      <Button type="submit" color="primary" filled>
                        <strong>Register</strong>
                      </Button>
                    </Column>
                  </Row>
                </form>
              </div>
            </div>
          </Column>
        </Row>
      </div>
    </section>
  );
}

Register.propTypes = {
  setMessage: func.isRequired,
  colorizeRed: func.isRequired
};

export default Register;
