import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Button from '../Button';

describe('Button: Component', () => {
  it('Should Component Renders', () => {
    const { queryByText } = render(
      <Button>
        Lorem Ipsum
      </Button>
    );

    expect(queryByText('Lorem Ipsum')).toBeInTheDocument();
  });

  it('Should Click Trigger Works', () => {
    const handleClick = jest.fn();

    const { getByRole } = render(
      <Button onClick={handleClick}>
        Lorem Ipsum
      </Button>
    );

    const button = getByRole('button');

    fireEvent.click(button);

    expect(handleClick).toHaveBeenCalled();
  });
});
