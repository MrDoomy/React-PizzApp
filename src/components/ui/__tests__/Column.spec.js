import React from 'react';
import { render, screen } from '@testing-library/react';
import Column from '../Column';

describe('Column: Component', () => {
  it('Should Component Renders', () => {
    render(
      <Column>
        Lorem Ipsum
      </Column>
    );

    expect(screen.queryByText('Lorem Ipsum')).toBeInTheDocument();
  });
});
