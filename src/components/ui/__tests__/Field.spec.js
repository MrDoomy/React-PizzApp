import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Field from '../Field';

describe('Field: Component', () => {
  it('Should Component Renders', () => {
    const { queryByPlaceholderText } = render(
      <Field
        name="test"
        placeholder="Test"
        defaultValue="Lorem Ipsum"
        onChange={jest.fn()} />
    );

    expect(queryByPlaceholderText('Test')).toBeInTheDocument();
  });

  it('Should Change Trigger Works', () => {
    const handleChange = jest.fn();

    const { getByPlaceholderText, queryByDisplayValue } = render(
      <Field
        name="test"
        placeholder="Test"
        onChange={handleChange} />
    );

    const input = getByPlaceholderText('Test');

    fireEvent.change(input, { target: { value: 'Lorem Ipsum' }});

    expect(handleChange).toHaveBeenCalled();

    expect(queryByDisplayValue('Lorem Ipsum')).toBeInTheDocument();
  });
});
