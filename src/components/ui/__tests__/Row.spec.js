import React from 'react';
import { render, screen } from '@testing-library/react';
import Row from '../Row';

describe('Row: Component', () => {
  it('Should Component Renders', () => {
    render(
      <Row>
        Lorem Ipsum
      </Row>
    );

    expect(screen.queryByText('Lorem Ipsum')).toBeInTheDocument();
  });
});
