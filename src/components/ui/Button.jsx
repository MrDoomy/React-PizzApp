import React from 'react';
import { string, bool, func, node } from 'prop-types';
import classNames from 'classnames';

function Button(props) {
  const { color, lighten, outlined, filled, type, onClick, disabled, children } = props;

  return (
    <button
      className={classNames('button', {
        [`is-${color}`]: color,
        'is-light': lighten,
        'is-outlined': outlined,
        'is-fullwidth': filled
      })}
      type={type}
      onClick={onClick}
      disabled={disabled}>
      {children}
    </button>
  );
}

Button.defaultProps = {
  type: 'text'
};

Button.propTypes = {
  color: string,
  lighten: bool,
  outlined: bool,
  filled: bool,
  type: string,
  onClick: func,
  disabled: bool,
  children: node.isRequired
};

export default Button;
