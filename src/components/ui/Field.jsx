import React from 'react';
import { string, oneOfType, number, func, bool } from 'prop-types';
import classNames from 'classnames';

function Field(props) {
  const { label, name, type, placeholder, defaultValue, onChange, onBlur, error } = props;

  return (
    <div className="field">
      {label && <label className="label">{label}</label>}
      <div className="control">
        <input
          className={classNames('input', { 'is-danger': error })}
          name={name}
          type={type}
          placeholder={placeholder}
          defaultValue={defaultValue}
          onChange={onChange}
          onBlur={onBlur} />
      </div>
      {error && <p className="help is-danger">{error}</p>}
    </div>
  );
}

Field.defaultProps = {
  type: 'text'
};

Field.propTypes = {
  label: string,
  name: string.isRequired,
  type: string,
  placeholder: string.isRequired,
  defaultValue: oneOfType([string, number]),
  onChange: func.isRequired,
  onBlur: func,
  error: oneOfType([string, bool])
};

export default Field;
