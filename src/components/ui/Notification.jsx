import React, { useState } from 'react';
import { func } from 'prop-types';
import classNames from 'classnames';

function useShortMessage(delay) {
  const [value, setValue] = useState('');

  const resetValue = () => setValue('');

  const setMessage = message => {
    setValue(message);

    setTimeout(() => {
      resetValue();
    }, delay);
  };

  return [
    value,
    setMessage,
    resetValue
  ];
}

function Notification(props) {
  const [color, setColor] = useState('');
  const [message, setMessage, resetMessage] = useShortMessage(3000);

  return (
    <>
      {message && (
        <div className={
            classNames('notification', {
              'is-danger': color === 'red',
              'is-success': color === 'green',
              'is-info': color === 'blue',
              'is-warning': color === 'yellow'
            })}
            style={{ position: 'fixed', bottom: 0, right: '1.5rem', zIndex: 5 }}>
          <button className="delete" onClick={resetMessage} />
          <strong>{message}</strong>
        </div>
      )}
      {props.render({
        setMessage,
        colorizeRed: () => setColor('red'),
        colorizeGreen: () => setColor('green'),
        colorizeBlue: () => setColor('blue'),
        colorizeYellow: () => setColor('yellow')
      })}
    </>
  );
}

Notification.propTypes = {
  render: func.isRequired
};

export default Notification;
