import React from 'react';
import { number, node } from 'prop-types';
import classNames from 'classnames';

function Column(props) {
  const { size, offset, children } = props;

  return (
    <div className={classNames('column', { [`is-${size}`]: size, [`is-offset-${offset}`]: offset })}>
      {children}
    </div>
  );
}

Column.propTypes = {
  size: number,
  offset: number,
  children: node
};

export default Column;
