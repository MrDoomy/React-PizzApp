import React from 'react';
import { node } from 'prop-types';

function Row(props) {
  return (
    <div className="columns">
      {props.children}
    </div>
  );
}

Row.propTypes = {
  children: node.isRequired
};

export default Row;
