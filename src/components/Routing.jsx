import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Home, Login, Register, Settings, Pizza } from './containers';
import { Notification } from './ui';

function Routing() {
  return (
    <Switch>
      <Route exact path="/">
        <Notification render={props => (
          <Home {...props} />
        )} />
      </Route>
      <Route path="/login">
        <Notification render={props => (
          <Login {...props} />
        )} />
      </Route>
      <Route path="/register">
        <Notification render={props => (
          <Register {...props} />
        )} />
      </Route>
      <Route path="/settings">
        <Settings />
      </Route>
      <Route path="/pizza/:id">
        <Pizza />
      </Route>
    </Switch>
  );
}

export default Routing;
