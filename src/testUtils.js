import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { makeStore } from './redux/store';

export function renderWithRouter(ui) {
  return {
    ...render(
      <Router>
        {ui}
      </Router>
    )
  };
}

export function renderWithRedux(ui, initialState = {}) {
  const store = makeStore(initialState);

  return {
    ...render(
      <Provider store={store}>
        {ui}
      </Provider>
    ),
    store
  };
}

export function renderWithReduxAndRouter(ui, initialState = {}) {
  const store = makeStore(initialState);

  return {
    ...render(
      <Provider store={store}>
        <Router>
          {ui}
        </Router>
      </Provider>
    ),
    store
  }
}
