# PizzApp

> Version 0.9.0

## Intro

WebApp based on pizza management to learn the **React** framework.

This _frontend_ application was built using following technologies,

- Bulma
- React

### Context

- [x] ~~Statefull Component Syntax~~
- [x] Stateless Component Syntax
- [x] Handling State & Props
- [x] Handling Form & Events
- [x] Async Call (**Render Props** Pattern)
- [x] React Router
- [x] Dumb Vs Smart Components
- [x] Redux
- [x] Side Effects
- [x] _Keep Calm And Coding_
- [x] _All U Need Is Pizza_
- [x] Jest
- [ ] TypeScript

## Process

Repository:

```
git clone -b 0.9.0 https://gitlab.com/dmnchzl/react-pizzapp.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Build:

```
npm run build
npx serve -s build
```

Test:

```
npm run test
```

## Docs

- **Bulma**: A Free, _Open-Source_ CSS Framework (Based On **Flexbox**)
  - [https://bulma.io/](https://bulma.io/)

- **React**: A JavaScript Library For Building UI
  - [https://reactjs.org/](https://reactjs.org/)

- **React Router**: Declarative Routing For React
  - [https://reacttraining.com/react-router/](https://reacttraining.com/react-router/)

- **Redux**: Predictable State Container For JavaScript Apps
  - [https://redux.js.org/](https://redux.js.org/)

- **Jest**: A Delightful JavaScript Testing Framework
  - [https://jestjs.io/](https://jestjs.io/)

- **Testing Lib**: Simple & Complete Testing Utilities
  - [https://testing-library.com/](https://testing-library.com/)

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
